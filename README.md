# Soggy Goo

Numbers only.

## Name

One idea for naming is to use the fact that some of the glyphs
look like letters, so it may be possible to spell the name of
the font using its number glyphs.

Could be GOO = 600, or SOY = 507.

approximately:

- 0 = O
- 1 = L or I (both a stretch)
- 5 = S
- 6 = G (a bit)
- 7 = Y (a bit)
- 9 = a (a bit)

- SLOG = 5106
- SOGGY GOO = 50667 600
- OISI = 0151
- YOYO = 7070


## Notes

The /seven was found in the design file for Necker,
imported over here, then expanded into a larger design.

The /eight has been redesigned many times.

# END
